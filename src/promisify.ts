/**
 * Turn any function into a promise, whether it is async, sync or returns a promise.
 * @param cb The function or method.
 */
export const promisify =
	<T extends (...args: any[]) => ReturnType<T> extends Promise<infer PT> ? Promise<PT> : ReturnType<T>> (cb: T, thisArg?: any): (...args: Parameters<T>) => ReturnType<T> extends Promise<infer PT> ? Promise<PT> : Promise<ReturnType<T>> =>
		(...args: Parameters<T>): ReturnType<T> extends Promise<infer PT> ? Promise<PT> : Promise<ReturnType<T>> =>
			new Promise<ReturnType<T> extends Promise<infer PT> ? PT : ReturnType<T>>(async (resolve, reject) => {
				try
				{
					let value = cb.constructor.name === "AsyncFunction" ? await cb.apply(thisArg || {}, args) : cb.apply(thisArg || {}, args);
					if (value instanceof Promise) (value as ReturnType<T> as Promise<any>).then(resolve).catch(reject);
					else resolve(value as any);
				} catch (error)
				{
					reject(error);
				}
			}) as any;
