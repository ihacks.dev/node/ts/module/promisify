// Imports
import { promisify as mod } from "./promisify";

// Exports
module.exports = mod;
export const promisify = mod;
export default mod;
