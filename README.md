# @ihacks.dev/promisify

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/201cbcee696342e28c6fe5844db1d2c1)](https://www.codacy.com/gl/ihacks.dev/promisify) &nbsp;
[![pipeline status](https://gitlab.com/ihacks.dev/node/ts/module/promisify/badges/master/pipeline.svg)](https://gitlab.com/ihacks.dev/node/ts/module/promisify/-/commits/master) &nbsp;
[![coverage report](https://gitlab.com/ihacks.dev/node/ts/module/promisify/badges/master/coverage.svg)](https://gitlab.com/ihacks.dev/node/ts/module/promisify/-/commits/master)

> Ensures that a function returns a promise.

## Installation

```bash
yarn add @ihacks.dev/promisify
# or
npm i -S @ihacks.dev/promisify
```

## Usage

```typescript
import promisify from "@ihacks.dev/promisify";
const fn = promisify(() => "this is a sync function.");
await fn()
// this is a sync function.
```

## License

Copyright (c) 2020 James Bradlee

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
