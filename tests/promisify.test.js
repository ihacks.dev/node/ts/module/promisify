// Imports
const promisify = require("../build");

const err = new Error("I am an uncool error.");

const syncFn = () => {};
const asyncFn = async () => {};
const promiseFn = () => new Promise(async resolve => resolve());
const syncFnError = () => { throw err; };
const asyncFnError = async () => { throw err; };
const promiseFnError = () => new Promise(async (resolve, reject) => reject(err));

const createAndRunFor = (n,fn) => it("Promisifies and calls " + n, async () => {
	const _ = promisify(fn);
	expect(typeof _).toStrictEqual("function");
	try
	{
		await _();
	} catch (error)
	{
		expect(error).toBeInstanceOf(Error);
	}
});

createAndRunFor("syncFn", syncFn);
createAndRunFor("asyncFn", asyncFn);
createAndRunFor("promiseFn", promiseFn);
createAndRunFor("syncFnError", syncFnError);
createAndRunFor("asyncFnError", asyncFnError);
createAndRunFor("promiseFnError", promiseFnError);

